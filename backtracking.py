chess_board = {}
def initialize_board(n: int) -> None:
    chess_board["queen"] = {}
    chess_board["row"] = {}
    chess_board["col"] = {}
    chess_board["nwtose"] = {}
    chess_board["setone"] = {}

    for i in range(n):
        chess_board["queen"][i] = -1
        chess_board["row"][i] = 0
        chess_board["col"][i] = 0

    for i in range(-(n-1), n):
        chess_board["nwtose"][i] = 0

    for i in range(2*n - 1):
        chess_board["setone"][i] = 0


def free(row: int, col: int) -> bool:

    return chess_board["row"][row] == 0 and chess_board["col"][col] == 0 and \
        chess_board["nwtose"][col - row] == 0 and chess_board["setone"][col + row] == 0


def add_queen(row: int, col: int) -> None:
    chess_board["queen"][row] = col
    chess_board["row"][row] = 1
    chess_board["col"][col] = 1
    chess_board["nwtose"][col - row] = 1
    chess_board["setone"][col + row] = 1


def undo_queen(row: int, col: int) -> None:
    chess_board["queen"][row] = -1
    chess_board["row"][row] = 0
    chess_board["col"][col] = 0
    chess_board["nwtose"][col - row] = 0
    chess_board["setone"][col + row] = 0




def print_board(n: int) -> None:
    chess = [["-"]*n for _ in range(n)]
    for row in chess_board["queen"].keys():
        chess[row][chess_board["queen"][row]] = "Q"
    
    for i in chess: print(*i)


def place_queens(row: int) -> bool:

    n = len(chess_board["queen"].keys())

    for col in range(n):
        if free(row, col):
            add_queen(row, col)
            if row == n - 1:
                return True
            else:
                extended_soln = place_queens(row + 1)
            if extended_soln:
                return extended_soln
            else:
                undo_queen(row, col)

    return False



if __name__ == "__main__":

    n = int(input("Enter the number of queens : "))

    initialize_board(n)

    if place_queens(0):
        print_board(n)
