import random
class Sorting:
    def __init__(self,lst:list) -> None:
        self.lst = lst
        self.len = len(lst)


    def randomize(self):
        for i in range(self.len):
            fi,se = random.randrange(0,self.len), random.randrange(0,self.len)
            self.lst[fi],self.lst[se] = self.lst[se],self.lst[fi]

    def __merge(self,left: list,right: list):
        merged,m,n,i,j = [],len(left),len(right),0,0
        while i+j< m+n:
            if i == m :
                merged.append(right[j])
                j+=1
            elif j == n :
                merged.append(left[i])
                i+=1
            elif left[i] <= right[j]:
                merged.append(left[i])
                i+=1
            elif left[i] > right[j]:
                merged.append(right[j])
                j+=1
        return merged

    def __merge_sort(self,lst:list,left: int,right: int):
        if right - left<=1:
            return lst[left:right]
        if right - left > 1:
            mid = (left + right)//2
            L = self.__merge_sort(lst,left,mid)
            R = self.__merge_sort(lst,mid,right)

            return self.__merge(L,R)

    def merge_sort(self):
        return self.__merge_sort(self.lst,0,self.len)

    def __quick_sort(self,lst,left,right):

        if right - left <= 1:
            return
        
        rightMostPtr = left  + 1

        for ptr in range(left+1,right):
            if lst[ptr] <= lst[left]:
                lst[rightMostPtr],lst[ptr] = lst[ptr],lst[rightMostPtr]
                rightMostPtr+=1
        
        lst[left], lst[rightMostPtr - 1] = lst[rightMostPtr - 1] , lst[left]
        self.__quick_sort(lst,left,rightMostPtr - 1)
        self.__quick_sort(lst,rightMostPtr, right)

    def quick_sort(self):
        return self.__quick_sort(self.lst, 0 , self.len)
    

if __name__=="__main__":
    sorter = Sorting([random.randint(1,100) for _ in range(100)])
    sorter.randomize()
    print(sorter.lst)
    sorter.quick_sort()
    print(sorter.lst)
